//
//  ViewController.swift
//  Practica Alumnos
//
//  Created by Alberto Téllez on 28/05/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var studentTableView: UITableView!
    
    var students = [Student] ()
    var studentSelected: Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        studentTableView.register(UITableViewCell.self, forCellReuseIdentifier: "studentCellIdentifier")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        students = DataManager.dataManager.loadStudents()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editStudentSegue" {
            
            if let studentViewController = segue.destination as? StudentViewController {
                
                 studentViewController.delegate = self
                
                studentViewController.student = studentSelected
               
            }
        } else if segue.identifier == "addStudentSegue"  {
            
            if let studentViewController = segue.destination as? StudentViewController {
                
                studentViewController.delegate = self
            }
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let student = students[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCellIdentifier", for: indexPath)
        
        cell.textLabel?.text = student.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        studentSelected = students[indexPath.row]
        
        performSegue(withIdentifier: "editStudentSegue", sender: nil)

    }
}

extension ViewController: StudentViewControllerDelegate {
    
    func wasTappedOkButton() {
        
        students = DataManager.dataManager.loadStudents()
        studentTableView.reloadData()
        
        dismiss(animated: true, completion: nil)
    }
}

