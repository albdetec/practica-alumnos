//
//  StudentViewController.swift
//  Practica Alumnos
//
//  Created by Alberto Téllez on 28/05/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

protocol StudentViewControllerDelegate {
    
    func wasTappedOkButton()
}

class StudentViewController: UIViewController {
    
    var delegate: StudentViewControllerDelegate?
    
    var student: Student?
    
    @IBOutlet weak var registrationTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var surnameTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var approvedSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        guard let student = student else {
            
            return
        }
        registrationTxt.text = student.registration
        nameTxt.text = student.name
        surnameTxt.text = student.surname
        ageTxt.text = "\(student.age)"
        
        approvedSwitch.isOn = student.approved
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        
        guard let registration = registrationTxt.text, !registration.isEmpty, let name = nameTxt.text, !name.isEmpty else {  delegate?.wasTappedOkButton()
            return
        }
        
        let age = Int(ageTxt.text ?? "0") ?? 0
        
        let studentEdited = Student(registration: registration, name: name, surname: surnameTxt.text ?? "", age: age, approved: approvedSwitch.isOn)
        
        DataManager.dataManager.saveStudent(studentEdited)
        
        delegate?.wasTappedOkButton()
    }
}
