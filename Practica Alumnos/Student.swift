//
//  Student.swift
//  Practica Alumnos
//
//  Created by Alberto Téllez on 28/05/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation
import GRDB

struct Student {
    
    let registration: String
    let name: String
    let surname: String
    let age: Int
    let approved: Bool
}

extension Student: FetchableRecord, PersistableRecord  {
    
    init(row: Row) {
        
        registration = row["registration"]
        name = row["name"]
        surname = row["surname"]
        age = row["age"]
        approved = row["approved"]
    }
    
    func encode(to container: inout PersistenceContainer) {
        
        container["registration"] = registration
        container["name"] = name
        container["surname"] = surname
        container["age"] = age
        container["approved"] = approved
    }
}

